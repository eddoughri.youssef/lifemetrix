export default function checkInputs(input){
    const { developed, population, aids, infant, gdp, bmi, alcohol, schooling } = input;
    if(developed !== "Yes" && developed !== "No"){
        return 'Developed field must be Yes or No';
    }
    if(population === null || population === undefined || population === "" || isNaN(parseFloat(population))){
        return 'Population field must be a number';
    }
    if(parseFloat(population) < 1000000){
        return 'there is no country with population less than 1 million';
    }
    if(parseFloat(population) > 10000000000){
        return 'there is no country with population more than 10 billion';
    }
    if(aids === null || aids === undefined || aids === "" || isNaN(parseFloat(aids))){
        return 'Aids field must be a number';
    }
    if(parseFloat(aids) < 0){
        return 'Aids field must be a positive number';
    }
    if(parseFloat(aids) > 1000){
        return 'That is too much AIDS (have mercy !)';
    }
    if(infant === null || infant === undefined || infant === "" || isNaN(parseFloat(infant))){
        return 'Infant field must be a number';
    }
    if(parseFloat(infant) < 0){
        return 'Infant field must be a positive number';
    }
    if(parseFloat(infant) > 1000){
        return 'That is too much infant deaths (u monster !!!)';
    }
    if(gdp === null || gdp === undefined || gdp === "" || isNaN(parseFloat(gdp))){
        return 'GDP field must be a number';
    }
    if(parseFloat(gdp) < 0){
        return 'GDP field must be a positive number';
    }
    if(parseFloat(gdp) > 10000){
        return 'must be living in mars';
    }
    if(bmi === null || bmi === undefined || bmi === "" || isNaN(parseFloat(bmi))){
        return 'BMI field must be a number';
    }
    if(parseFloat(bmi) < 0){
        return 'BMI field must be a positive number';
    }
    if(parseFloat(bmi) > 100){
        return 'BMI field must be less than 100';
    }
    if(alcohol === null || alcohol === undefined || alcohol === "" || isNaN(parseFloat(alcohol))){
        return 'Alcohol field must be a number';
    }
    if(parseFloat(alcohol) < 0){
        return 'Are u sure u are not drunk ? Alcohol field must be a positive number';
    }
    if(parseFloat(alcohol) > 100){
        return 'with that much alcohol u will die before u can see the result';
    }
    if(schooling === null || schooling === undefined || schooling === "" || isNaN(parseFloat(schooling))){
        return 'Schooling field must be a number';
    }
    if(parseFloat(schooling) < 0){
        return 'Schooling field must be a positive number';
    }
    if(parseFloat(schooling) > 100){
        return "bruuuh... that's not how schooling works";
    }
    return null;
}