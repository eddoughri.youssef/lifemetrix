export default async function fetchResult({ developed, population, aids, infant, gdp, bmi, alcohol, schooling }) {
    const inputData = {
        'Status': developed,
        'Population': parseFloat(population),
        'HIV/AIDS': parseFloat(aids),
        'infant deaths': parseFloat(infant),
        'GDP': parseFloat(gdp),
        'BMI': parseFloat(bmi),
        'Alcohol': parseFloat(alcohol),
        'Schooling': parseFloat(schooling)
    }
    try{
        const res = await fetch(
            `https://dxhna2m8dc.execute-api.us-east-1.amazonaws.com/production`,{
                // disable cors
                method: 'POST',
                body: JSON.stringify({
                    data: inputData,
                }),
                headers: {
                    'Content-Type': 'application/text',
                },
            }
        );
        const data = await res.json();
        return {
            status: 'success',
            data: data.body,
        };
    }catch(err){
        return {
            status: 'error',
            data: err,
        };
    }
}
    
