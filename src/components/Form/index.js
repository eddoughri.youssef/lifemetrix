import React, { useEffect, useState } from 'react';
import {
    Box,
    Button,
    Stack,
    Text,
    Select,
    useToast,
  } from '@chakra-ui/react';


import InputText from '../Input/InputText';
import fetchResult from '../../utils/fetchResult';
import checkInputs from '../../utils/checkInputs';



import Header from '../Header';


export default function Form({loading, setLoading, setSubmitted, setResult}) {
  useEffect(()=>{
    console.log("Form");
    // get if existed the data from local storage
    const data = localStorage.getItem('data');
    if(data){
      const parsedData = JSON.parse(data);
      setDeveloped(parsedData.developed);
      setPopulation(parsedData.population);
      setAids(parsedData.aids);
      setInfant(parsedData.infant);
      setGdp(parsedData.gdp);
      setBmi(parsedData.bmi);
      setAlcohol(parsedData.alcohol);
      setSchooling(parsedData.schooling);
    }
  }, []);
  const makeDescription = (text, links) => {
    return (
      <Box
        display={'flex'}
        flexDirection={'column'}
        justifyContent={'start'}
        gap={4}
        mt={2}
      >
        <Text
          variant="p"
          color="black"
          fontFamily={'mono'}
          fontSize={{ base: 'xs', lg: 'sm' }}
        >
          {text}:
        </Text>
        {links && (
          <Stack
            direction={'column'}
            alignItems={'start'}
            px={2}
            spacing={2}
          >
            {links.map((link) => (
              <Button
                variant={'link'}
                color={'blue.400'}
                size={'sm'}
                key={link}
                onClick={() => {
                  window.open(link, '_blank');
                }}
              >
                {link}
              </Button>
            ))}
          </Stack>
        )}
    </Box>
    )
  }
  
  const simple = (value) => {
    return value;
  };
  // big numbers should handle numbers with 6 digits
  // that means adding , after 3 digits
  const bigNumberFormater = (value) => {
    // remove:
    //  all commas
    //  leading zeros
    //  any character that is not a float
    const str = value.toString().replace(/,/g, '').replace(/^0+/, '').replace(/[^\d]+/g, '');
    const len = str.length;
    if(len <= 3) return str;
    let res = '';
    let count = 0;
    for(let i = len - 1; i >= 0; i--){
      if(count === 3){
        res = `,${res}`;
        count = 0;
      }
      res = str[i] + res;
      count++;
    }
    return res;
  }
  const undoBigNumberFormater = (value) => {
    return parseInt(value.replace(/,/g, ''));
  }
  const toast = useToast();

  const [developed, setDeveloped] = useState('Yes');
  const [population, setPopulation] = useState("");
  const [aids, setAids] = useState("");
  const [infant, setInfant] = useState("");
  const [gdp, setGdp] = useState("");
  const [bmi, setBmi] = useState("");
  const [alcohol, setAlcohol] = useState("");
  const [schooling, setSchooling] = useState("");
  return (
    <Box
      w="100%"
      minH="100%"
      display="flex"
      justifyContent="center"
      alignItems="start"
      flexDirection={'column'}
    >
      <Stack
        rounded={'xl'}
        spacing={{ base: 2, md: 4, lg: 8 }}
        maxW={{ base: '100%', md: '90%' }}
      >
        <Header 
          title={"AgeXplorer"}
          description={"📝 Welcome to LifeMetrix! 🎉 Fill the form below for your life expectancy prediction. Optimize your health and plan ahead."}
          presentation={"Get started now!"}
          clicked={()=>{
            setDeveloped("No");
            setPopulation(37840044);
            setAids(2.46);
            setInfant(16.02);
            setGdp(3088);
            setBmi(26.2);
            setAlcohol(0.69);
            setSchooling(5.5);
          }}  
        />
        <Box as={'form'} mt={{base: '2', lg: '4'}}>
          <Stack spacing={4}>
            <Box display={'flex'} alignItems={'center'} gap={4}>
              <Text
                variant="label"
                color="black"
                fontFamily={'mono'}
                fontSize={{base: 'sm', lg: 'md'}}
              >
                Is your country consider a developed country?
              </Text>
              <Select
                bg={'gray.100'}
                border={0}
                color={'gray.500'}
                _placeholder={{
                  color: 'gray.500',
                }}
                value={developed}
                onChange={(e) => {
                  setDeveloped(e.target.value);
                }}
              >
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </Select>
            </Box>
            <InputText target={population} unformat={undoBigNumberFormater} format={bigNumberFormater} setTarget={setPopulation} label="Population" Description={makeDescription("Enter your country population", ['https://tinyurl.com/b7vvrkfh', 'https://tinyurl.com/55th9a4r'])} />
            <InputText target={aids} type={"number"} unformat={simple} format={simple} setTarget={setAids} label="AIDS deaths" Description={makeDescription("deaths caused by AIDS of the last 5-year-olds per 100,000 who were born alive in your country", ["https://ourworldindata.org/grapher/hiv-death-rates-by-age?tab=table", 'https://tinyurl.com/yfkz3zw8'])} />
            <InputText target={infant} type={"number"} unformat={simple} format={simple} setTarget={setInfant} label="Infant deaths" Description={makeDescription("The number of infant deaths in your country", ["https://tinyurl.com/msww87z3"])} />
            <InputText target={gdp} type={"number"} unformat={simple} format={simple} setTarget={setGdp} label="GDP" Description={makeDescription("Gross domestic product in your country per capita", ["https://www.worldometers.info/gdp/gdp-by-country/"])} />
            <InputText target={bmi} type={"number"} unformat={simple} format={simple} setTarget={setBmi} label="BMI" Description={makeDescription("The average body mass index of the entire population in your country", ["http://chartsbin.com/view/577"])} />
            <InputText target={alcohol} type={"number"} unformat={simple} format={simple} setTarget={setAlcohol} label="Alcohol" Description={makeDescription("Liters of alcohol consumption among people over 15 years old in your country", ["https://ourworldindata.org/alcohol-consumption"])} />
            <InputText target={schooling} type={"number"} unformat={simple} format={simple} setTarget={setSchooling} label="Schooling" Description={makeDescription("The avg number of years studied in your country", ["https://tinyurl.com/ynma56wj"])} />
          </Stack>
          <Button
            fontFamily={'heading'}
            mt={8}
            w={'full'}
            bgGradient="linear(to-r, red.400,pink.400)"
            color={'white'}
            _hover={{
              bgGradient: 'linear(to-r, red.400,pink.400)',
              boxShadow: 'xl',
            }}
            onClick={ async (e) => {
                e.preventDefault();
                const inputs = {developed ,population, aids, infant, gdp, bmi, alcohol, schooling};
                const diagnoseResult = checkInputs(inputs);
                if(diagnoseResult) {
                  toast({
                    title: "Error",
                    description: diagnoseResult,
                    status: "error",
                    isClosable: true,
                    duration: 5000,
                  })
                  return;
                }
                localStorage.setItem('data', JSON.stringify(inputs));
                setLoading(true);
                setSubmitted(true);
                let res = await fetchResult(inputs);
                console.log("result;",res);
                if(res.status === 'error' || isNaN(parseFloat(res.data))){
                  toast({
                    title: "Backend Warning",
                    description: "Backend Infrastucture is built, please wait for a few seconds.",
                    status: "warning",
                    isClosable: true,
                    duration: 5000,
                  });
                  res = await fetchResult(inputs);
                }
                setLoading(false);
                if(res.status === 'error' || isNaN(parseFloat(res.data))){
                  setResult("Server Faileur 😭 --");
                  return;
                }
                setResult(res.data);
            }}
          >
            {loading ? 'Loading...' : 'Predict'}
          </Button>
        </Box>
        form
      </Stack>
    </Box>
  );
}
